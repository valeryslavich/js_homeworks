const passwordField = document.querySelector("#password");
const checkPasswordField = document.querySelector("#check-password");
const enterBtn = document.querySelector(".btn");
const formError = document.querySelector('.text-alert');
const form = document.querySelector('.password-form');

form.addEventListener('click', (e) => {
    e.preventDefault()
    if(e.target.tagName === 'I') {
        e.target.classList.toggle( 'fa-eye-slash' );
        let inputId  = e.target.previousElementSibling
        if(inputId.type === "password"){
            inputId.type = 'text'
        } else {
            inputId.type = 'password'
        }
    }
})
enterBtn.addEventListener('click', (e)=> {
    e.preventDefault()
    if(passwordField.value === checkPasswordField.value) {
        formError.innerText = "Успішно"
        formError.style.color = "#32d932"

    } else {
        formError.innerText = "Паролі не співпадають"
        formError.style.color = "#ff0000"
    }

})