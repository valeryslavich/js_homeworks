let counter = 1
let stopbtn = document.querySelector('.stop')
let startbtn = document.querySelector('.start')
startbtn.setAttribute('disabled','disabled') 
const imageTimers = () => {
    let showImage = setInterval (()=>{
        if (counter > 4) {
        counter = 1
        }
        document.querySelector('.image-to-show').src = `./img/${counter}.jpg`
        counter++ 
    }, 3000);

stopbtn.addEventListener('click', ()=>{
    clearInterval(showImage)
    startbtn.removeAttribute('disabled')
})
}
imageTimers ()

startbtn.addEventListener('click', ()=>{
imageTimers()
startbtn.setAttribute('disabled','disabled')    
})
