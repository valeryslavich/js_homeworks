const  tabsFn= ()=>{

    const changeFn= (event)=>{
        let menuItems= document.querySelectorAll('[data-tab]')
        menuItems.forEach(item=>{
            if (item.classList.contains('active')){
                item.classList.remove('active')
            }
        })

        if (!event.target.classList.contains('active')) {
            event.target.classList.add('active')
        }
        let id = event.target.getAttribute('data-tab')
        let conteiners = document.querySelectorAll('.tab-choice')
        conteiners.forEach(item=>{
            if (item.classList.contains('active')){
                item.classList.remove('active')
            }
        })
        if (!document.getElementById(id).classList.contains("active")) {
            document.getElementById(id).classList.add("active")
        }
    }

    let tabs = document.querySelector('.tab-list')
    tabs.addEventListener('click', (event)=>{
        if (event.target.nodeName ==="BUTTON") {
            changeFn(event)
        }
    })
}
tabsFn()

function Gallery() {
	const cards = [
		{
			data: 'Graphic Design',
			src: './image/section-works/Layer 1.png'
		},{
			data: 'Web Design',
			src: './image/section-works/Layer 2.png'
		},{
			data: 'Landing Pages',
			src: './image/section-works/Layer 3.png'
		},{
			data: 'Wordpress',
			src: './image/section-works/Layer 4.png'
		},{
			data: 'Graphic Design',
			src: './image/section-works/Layer 5.png'
		},{
			data: 'Web Design',
			src: './image/section-works/Layer 6.png'
		},{
			data: 'Landing Pages',
			src: './image/section-works/Layer 7.png'
		},{
			data: 'Wordpress',
			src: './image/section-works/Layer 8.png'
		},{
			data: 'Graphic Design',
			src: './image/section-works/Layer 9.png'
		},{
			data: 'Web Design',
			src: './image/section-works/Layer 10.png'
		},{
			data: 'Landing Pages',
			src: './image/section-works/Layer 11.png'
		},{
			data: 'Wordpress',
			src: './image/section-works/Layer 1.png'
		},
	]

const gallaryMenuWrapper = document.querySelector('.tab-list-gallery')
const loadMore = document.querySelector('.button-add')
const galleryContent = document.querySelector('.image-wrap')

gallaryMenuWrapper.addEventListener('click', (event)=>{
	let dataMenu =null
	const dataGallery = document.querySelectorAll('.image-item')
	 if (event.target.nodeName ="BUTTON") {
		dataMenu = event.target.getAttribute('data-menu')
	 }

	let menuItems = document.querySelectorAll( '[data-menu]' );

		menuItems.forEach( item => {
			if ( item.classList.contains( 'active' ) ) {
				item.classList.remove( 'active' );
			}
		} );

		if ( !event.target.classList.contains( 'active' ) ) {
			event.target.classList.add( 'active' );
		}

		dataGallery.forEach( (item) => {
			if (dataMenu !== item.getAttribute('data-gallery') && dataMenu !== 'all') {
				item.style.display ='none'
			} else {
				item.style.display ='block'
			}
		})
})
let counter = 0;

loadMore.addEventListener('click',()=>{
	counter++
	let loading = document.querySelector('.lds-ripple')
	loading.style.display = 'inline-block'
	loadMore.style.display = 'none'
	// loadMore.classList.add('lds-ring')
	// loadMore.setAttribute ('disabled', '')

	setTimeout(()=>{
		loading.style.display = 'none'
		loadMore.style.display = 'block'
		// loadMore.innerText.remove()
		// loadMore.classList.remove('lds-ring')
		// loadMore.remove()
		// loadMore.innerHTML = '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>'
		// loadMore.removeAttribute('disabled')
		cards.forEach((image, id)=>{
			galleryContent.insertAdjacentHTML('beforeend', `
				<div class="image-item" data-gallery="${image.data}">
					<div class="item-hover">
						<div class="item-content">
							<div class="item-links">
								<a href="#" class="link1"><img src="./image/section-works/Combined shape 7431.svg" alt=""></a>
								<a href="#" class="link2"></a>
							</div>
							<div class="item-title">creative design</div>
							<div class="item-desc">${image.data}</div>
						</div>
					</div>
					<img src="${image.src}" alt="" class="image">
				</div>`
			)
		})

		if (counter === 2) {
			loadMore.remove()
		}

	}, 2000);
})
}
document.addEventListener('DOMContentLoaded', () => {
	Gallery();
});

const thumbs = new Swiper ('.thumbs-slider', {
	spaceBetween : 36,
	slidesRerView: 'auto',
	freeMode : true,
	watchSlidesProgress: true,
});

const swiperMain = new Swiper ('.slider', {
	spaceBetween : 10,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	thumbs: {
		swiper: thumbs
	},
})


function imageGallery() {
	const images = [
		{
			src: './image/section-gallery/vanglo-house-1 1.png'
		}, {
			src: './image/section-gallery/vanglo-house-6 1.png'
		}, {
			src: './image/section-gallery/ringve-museum-1 1.png'
		}, {
			src: './image/section-gallery/80493541_1281644acb_o 1.png'
		}, {
			src: './image/section-gallery/p1_8 1.png'
		}, {
			src: './image/section-gallery/vanglo-house-1 2.png'
		}, {
			src: './image/section-gallery/ringve-museum-1 1.png'
		}, {
			src: './image/section-gallery/Kids-Store-Lighting-2 2.png'
		}, {
			src: './image/section-gallery/ia-Pools-1 1.png'
		}, {
			src: './image/section-gallery/Kids-Store-Lighting-1 (1) 1.png'
		}, {
			src: './image/section-gallery/vanglo-house-6 2.png'
		}, {
			src: './image/section-gallery/ia-Pools-1 1.png'
		}, {
			src: './image/section-gallery/Kids-Store-Lighting-1 (1) 1.png'
		}, {
			src: './image/section-gallery/vanglo-house-6 2.png'
		}, {
			src: './image/section-gallery/ia-Pools-1 1.png'
		}, {
			src: './image/section-gallery/Kids-Store-Lighting-1 (1) 1.png'
		}, {
			src: './image/section-gallery/vanglo-house-6 2.png'
		}, {
			src: './image/section-gallery/7328272788_c5048326de_o 1.png'
		}
	]	
let btngallery = document.querySelector('.button-gallery')
let galleryImage = document.querySelector('.content-wrap')
let counter = 0;
btngallery.addEventListener('click',()=>{
	counter++

	// loadMore.classList.add('loader')
	btngallery.setAttribute ('disabled', '')

	setTimeout(()=>{
		// loadMore.classList.remove('loader')
		btngallery.removeAttribute('disabled')
		if (counter === 2) {
			btngallery.remove()
		}
	
		images.forEach((image, id)=>{
			galleryImage.insertAdjacentHTML('beforeend', `
				<div class="image-wrap-gal">
					<img src="${image.src}" alt="" class="image">
				</div>`
			)
		})
	}, 1500);
})
}
document.addEventListener('DOMContentLoaded', () => {
	imageGallery();
});