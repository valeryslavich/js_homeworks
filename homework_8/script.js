// В деяких умовах завдань є помилки (наприклад елементів з класом section-title немає), тому я виправив умови

const allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach((item) => {
    item.style.backgroundColor = '#ff0000'
})

const list = document.getElementById ("optionsList");
console.log(list);
console.log(list.parentElement);
console.log(list.childNodes);

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = 'This is a paragraph';

const mainHeader = document.querySelector(".main-header");
console.log(mainHeader);
[...mainHeader.children].forEach((item) => {
    item.classList.add("nav-item");
});

const productItemTitle = document.querySelectorAll(".product-item-title");
productItemTitle.classList.remove("product-item-title");