/* Асихронність y JS це можливість робити кілька речей одночасно, не зупиняючи і не блокуючи основний потік. 
   Якщо ми виконуємо операцію, яка вимагає більше часу, наприклад, запити, тоді, краще виштовхнути це з основного потоку 
   і виконати завдання асинхронно. Для асихронності використовуються Promises i async/await.
*/

const container = document.querySelector ('.container');

const func = async () => {
const userIP = await fetch("https://api.ipify.org/?format=json").then(data => data.json())
console.log(userIP)}

const info =  async () =>{
    await fetch("http://ip-api.com/json/", {"query": "${res.ip}"}).then(data => data.json()).then(info => {container.insertAdjacentHTML ('beforeend', `
    <span>country: ${info.country} , region: ${info.regionName} , city: ${info.city} .</span>`)})}


