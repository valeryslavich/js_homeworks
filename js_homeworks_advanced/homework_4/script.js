/*Це підхід звернення до сервера за допомогою JS без перезавантаження сторінки. Дана технологія економить час*/

const filmContainer = document.querySelector ('.js-film');
const characterContainer = document.querySelector ('.js-character');
const ul = document.querySelector('ul');
const loading = document.querySelector ('.lds-roller')

fetch ('https://ajax.test-danit.com/api/swapi/films')
.then (res => res.json())
.then (films => {
    console.log (films);
    films.forEach (film => {
        filmContainer.insertAdjacentHTML ('beforeend', `<li>Name: ${film.name}, Episode ID#: ${film.episodeId}, Intro: ${film.openingCrawl}</li>`)
        ul.innerHTML = 'Loading ...';
        film.characters.forEach (linksCh => {
            fetch(linksCh).then(res => res.text()).then(data => {characterContainer.insertAdjacentHTML ('beforeend', `<li>${data}<li>`),  ul.innerHTML = '', loading.style.display = "none"})   
        })
    })
})
.catch (err => {console.error ('Error', err)})