const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
  const div = document.createElement('div')
  div.setAttribute('id','root')
  document.body.prepend(div)
  const addList = () => {
        books.map(({author, name, price,})=>{
          console.log({author, name, price,});
          try{
            if (!name) {
              throw new Error("Дані неповні: немає назви");
            }
            if (!author) {
              throw new Error("Дані неповні: немає автора");
            }
            if (!price) {
              throw new Error("Дані неповні: немає ціни");
            }
            div.insertAdjacentHTML('afterbegin', `
            <ul>
              <li>Автор: ${author}</li>
              <li>Название: ${name}</li>
              <li>Цена: ${price}</li>
            </ul>
          `)
          }
            catch(error){
              console.log(error)
            };
        })
  }
  addList (books)
  