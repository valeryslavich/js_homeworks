class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(value) {
        this._name = value;
    }
    get name() {
        return this._name;
    }
    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }
    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return this._salary;
    }
}

let employee = new Employee("Valerii", 28, 2000);

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    set salary(value) {
        this._salary = value * 3
    }

    get salary() {
        return this._salary * 3;
    }
}

const frontDev = new Programmer("Oleg", 30, 1800, "eng");
frontDev.salary = 1200;
console.log(frontDev);

const backDev = new Programmer("Ivan", 36, 3100, "it");
console.log(backDev);