// Завдання
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

const colorBtn = () => {
    let btn = document.querySelectorAll(".btn")
    // console.log(btn);

    document.addEventListener('keypress', (event) => {
        for (let item of btn) {
            item.style = "background:black"
            if (item.innerText.toUpperCase() == event.key.toUpperCase()) {
                item.style = "background:blue"
            }
        }
        // console.log(event.key);
    })

}
colorBtn()
