const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.body;
const func = (arr, parent) => {
    const ul = document.createElement("ul")

    parent.append(ul)
     arr.forEach(items =>{
        ul.insertAdjacentHTML('afterbegin',`<li>${items}</li>`)
    })
}
func(arr, parent);
