const tabItem = document.querySelectorAll(".tabs-title");
const tabs = document.querySelector('.tabs');
const tabContainer = document.querySelectorAll('.tab-content');


const toggleCssClass = (nodeArr, cssClass, activeElement) => {
    nodeArr.forEach((nodeItem) => {
        if(nodeItem.classList.contains(cssClass)) {
            nodeItem.classList.remove(cssClass)
        }
    })
    activeElement.classList.add(cssClass);
}

const tabsFn = (event) => {
    const id = event.target.getAttribute('data-tab');
    const activeElement = document.getElementById(id)
    toggleCssClass(tabItem,  'active', event.target)
    toggleCssClass(tabContainer, 'active', activeElement)
}

tabs.addEventListener('click', (event)=>{
    if(event.target.nodeName === 'LI') {
        tabsFn(event)
    }
})