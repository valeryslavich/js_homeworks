const filteredArray = ['hello', 'world', 23, '23', null]

const filterBy = (array, type) => {
    return array.filter((item)=>typeof item !== type)
}

console.log(filterBy(filteredArray, "string"));

console.log(filteredArray.includes(23))